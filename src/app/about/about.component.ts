import { Component, OnInit } from '@angular/core';
import { PlayList } from '../core/store/state/playlist.model';
import { FormBuilder } from '@angular/forms';

type FormModel<T> = { [P in keyof T]: any };

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {
  form = this.initForm();

  constructor(
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() { }

  initForm() {
    const form: FormModel<PlayList> = {
      artwork: [''],
      kind: [''],
      curatorName: [''],
      id: [''],
      name: [''],
      url: ['']
    };
    return this.formBuilder.group(form);
  }

  submitForm() {
    // dispatch action to store here
    alert(JSON.stringify(this.form.value));
  }

}
