import { HttpClientModule } from '@angular/common/http';
import { async, TestBed } from '@angular/core/testing';
import { MatSnackBarModule } from '@angular/material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { NgxsModule, Store } from '@ngxs/store';
import { of } from 'rxjs';

import { PlaylistService } from '../../services/playlist.service';
import { FilterPlaylist, GetPlaylist } from './playlist.actions';
import { PlayList, PlayListApiModel } from './playlist.model';
import { PlaylistSelectors } from './playlist.selectors';
import { PlaylistState, PlaylistStateModel } from './playlist.state';

const playlistsRes: PlayListApiModel[] = [
  {
    artwork: 'https://is1-ssl.mzstatic.com/image/thumb/Features117/v4/94/17/2e/94172e16-e5d7-2d2b-66ec-e72473988168/source/600x600cc.jpg',
    curator_name: 'Apple Music Pop',
    id: 'pl.f4d106fed2bd41149aaacabb233eb5eb',
    kind: 'playlist',
    name: 'Todays Hits',
    url: 'https://music.apple.com/us/playlist/todays-hits/'

  },
  {
    artwork: 'https://is3-ssl.mzstatic.com/image/thumb/Features128/v4/be/ca/be/becabe5c-150a-3dd7-a0d6-a39edc1760a2/source/600x600cc.jpg',
    curator_name: 'Apple Music Hip-Hop',
    id: 'pl.abe8ba42278f4ef490e3a9fc5ec8e8c5',
    kind: 'playlist',
    name: 'The A-List: Hip-Hop',
    url: 'https://music.apple.com/us/playlist/the-a-list-hip-hop/'

  }
];

const mockPlaylistService: Partial<PlaylistService> = {
  getPlaylists: () => {
    const mockRes = {
      featuredPlaylists: {
        name: 'featuredPlaylists',
        content: playlistsRes
      }
    };
    return of(mockRes);
  }
}

const getState = () => ({
  playlists: playlistsRes.map(PlayList.fromPlayListApiModel),
  query: '',
  isLoading: false
} as PlaylistStateModel);

describe('People state', () => {
  let store: Store;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        NgxsModule.forRoot([PlaylistState]),
        HttpClientModule,
        MatSnackBarModule,
        NoopAnimationsModule
      ],
      providers: [
        {
          provide: PlaylistService,
          useValue: mockPlaylistService
        }
      ]
    }).compileComponents();
    store = TestBed.get(Store);
  }));

  describe('Actions', () => {

    describe('GetPlaylist action', () => {

      it('should request a list of people and update the state with the FE models', () => {
        store.select((state: { playlist: PlaylistStateModel }) => state.playlist.playlists)
          .subscribe(playlists => {
            playlists.forEach((playlist, i) => {
              expect(playlist.artwork).toEqual(playlistsRes[i].artwork);
              expect(playlist.curatorName).toEqual(playlistsRes[i].curator_name);
              expect(playlist.id).toEqual(playlistsRes[i].id);
              expect(playlist instanceof PlayList);
            });
          });
        store.dispatch(new GetPlaylist());
      });
    });

    describe('FilterPlaylist action', () => {

      it('should set the query in the state', () => {
        store.dispatch(new FilterPlaylist('test'));
        store.select((state: { playlist: PlaylistStateModel }) => state.playlist.query)
          .subscribe(query => expect(query).toBe('test'));
      });
    });
  });

  describe('Selectors', () => {

    it('should return the count of playlists', () => {
      expect(PlaylistSelectors.playlistCount(getState())).toBe(2);
    });

    it('should return the playlists', () => {
      PlaylistSelectors.playlists(getState()).forEach((playlist, i) => {
        expect(playlist.artwork).toEqual(getState().playlists[i].artwork);
        expect(playlist.id).toEqual(getState().playlists[i].id);
      });
    });

    it('should return the playlists filtered', () => {
      const newState = {
        ...getState(),
        query: 'The A-List: Hip-Hop'
      };

      const result = PlaylistSelectors.playlists(newState);
      expect(result.length).toBe(1);
      expect(result[0].name).toBe('The A-List: Hip-Hop');
    });

  });
});
