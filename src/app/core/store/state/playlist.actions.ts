export class GetPlaylist {
  static readonly type = '[Playlist] get playlist';
}

export class FilterPlaylist {
  static readonly type = '[Playlist] filter playlist';
  constructor(public query: string) {}
}
