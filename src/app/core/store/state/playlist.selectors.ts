import { Selector } from "@ngxs/store";
import { PlaylistStateModel, PlaylistState } from "./playlist.state";

export class PlaylistSelectors {


  @Selector([PlaylistState])
  static playlists({ playlists, query }: PlaylistStateModel) {
    return query === ''
      ? playlists
      : playlists.filter(playlist => playlist.name.toLowerCase().includes(query.toLowerCase()));
  }

  @Selector([PlaylistSelectors.playlists])
  static playlistCount(abb) {
    return abb.length;
  }

  @Selector([PlaylistState])
  static isLoading({ isLoading }: PlaylistStateModel) {
    return isLoading;
  }
}
