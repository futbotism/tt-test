import { Action, Selector, State, StateContext } from '@ngxs/store';
import { map, catchError, finalize } from 'rxjs/operators';

import { GetPlaylist, FilterPlaylist } from './playlist.actions';
import { PlaylistService } from '../../services';
import { PlayList } from './playlist.model';
import { MatSnackBar } from '@angular/material';
import { EMPTY } from 'rxjs';

export class PlaylistStateModel {
  public playlists: PlayList[];
  public query: string;
  public isLoading: boolean;
}

@State<PlaylistStateModel>({
  name: 'playlist',
  defaults: {
    playlists: [],
    query: '',
    isLoading: false
  }
})
export class PlaylistState {

  constructor(
    private playlistService: PlaylistService,
    private matSnackBar: MatSnackBar,
  ) { }

  @Action(GetPlaylist)
  getPlaylist({ patchState, dispatch }: StateContext<PlaylistStateModel>) {
    patchState({ isLoading: true });
    return this.playlistService
      .getPlaylists()
      .pipe(
        map(playlists => playlists.featuredPlaylists.content.map(PlayList.fromPlayListApiModel)),
        map(playlists => patchState({ playlists })),
        finalize(() => patchState({ isLoading: false })),
        catchError(() => {
          this.matSnackBar.open('There was an error getting the playlist', 'Retry').onAction().subscribe(() => dispatch(GetPlaylist));
          return EMPTY;
        })
      );
  }

  @Action(FilterPlaylist)
  filterPlaylist({ patchState, dispatch }: StateContext<PlaylistStateModel>, { query }: FilterPlaylist) {
    patchState({ query });
  }
}
