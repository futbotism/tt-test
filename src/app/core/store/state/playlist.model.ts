export interface PlayListApiModel {
  id: string;
  kind: string;
  name: string;
  url: string;
  curator_name: string;
  artwork: string;
}

export class PlayList {

  constructor(
    public id: string,
    public kind: string,
    public name: string,
    public url: string,
    public curatorName: string,
    public artwork: string
  ) { }

  static fromPlayListApiModel(playListApiModel: PlayListApiModel) {
    return new PlayList(
      playListApiModel.id,
      playListApiModel.kind,
      playListApiModel.name,
      playListApiModel.url,
      playListApiModel.curator_name,
      playListApiModel.artwork
    );
  }

}
