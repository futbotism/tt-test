import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PlayListApiModel } from '../store/state/playlist.model';
import { ApiResponse } from 'src/app/shared';

export interface PlaylistResponse { featuredPlaylists: ApiResponse<PlayListApiModel>; }

@Injectable({
  providedIn: 'root'
})
export class PlaylistService {

  constructor(
    private httpClient: HttpClient
  ) { }

  getPlaylists() {
    const endpoint = 'https://portal.organicfruitapps.com/programming-guides/v2/us_en-us/featured-playlists.json';
    return this.httpClient.get<PlaylistResponse>(endpoint);
  }

}
