import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NgxsModule } from '@ngxs/store';

import { SharedModule } from '../shared/shared.module';
import { CoreComponent } from './core.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { PlaylistCounterComponent } from './playlist-counter/playlist-counter.component';
import { PlaylistState } from './store/state/playlist.state';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    HttpClientModule,
    NgxsModule.forRoot([PlaylistState])
  ],
  declarations: [
    CoreComponent,
    HeaderComponent,
    FooterComponent,
    PlaylistCounterComponent
  ]
})
export class CoreModule { }
