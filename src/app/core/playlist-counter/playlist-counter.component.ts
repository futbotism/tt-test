import { Component, OnInit } from '@angular/core';
import { PlaylistState } from '../store/state/playlist.state';
import { Store } from '@ngxs/store';
import { PlaylistSelectors } from '../store/state/playlist.selectors';

@Component({
  selector: 'app-playlist-counter',
  templateUrl: './playlist-counter.component.html',
  styleUrls: ['./playlist-counter.component.scss']
})
export class PlaylistCounterComponent implements OnInit {
  playlistCount = this.store.select(PlaylistSelectors.playlistCount);

  constructor(
    private store: Store
  ) { }

  ngOnInit() {
  }

}
