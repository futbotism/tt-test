import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlaylistCounterComponent } from './playlist-counter.component';
import { NgxsModule } from '@ngxs/store';

describe('PlaylistCounterComponent', () => {
  let component: PlaylistCounterComponent;
  let fixture: ComponentFixture<PlaylistCounterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ NgxsModule.forRoot() ],
      declarations: [ PlaylistCounterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaylistCounterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
