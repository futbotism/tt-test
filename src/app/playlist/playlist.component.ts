import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';

import { FilterPlaylist, GetPlaylist } from '../core/store/state/playlist.actions';
import { PlaylistSelectors } from '../core/store/state/playlist.selectors';

@Component({
  selector: 'app-playlist',
  templateUrl: './playlist.component.html',
  styleUrls: ['./playlist.component.scss']
})
export class PlaylistComponent implements OnInit {
  query: string;
  playlists = this.store.select(PlaylistSelectors.playlists);
  isLoading = this.store.select(PlaylistSelectors.isLoading);

  constructor(
    private store: Store
  ) { }

  ngOnInit() {
    this.store.dispatch([new GetPlaylist]);
  }

  setQuery() {
    this.store.dispatch(new FilterPlaylist(this.query));
  }

}
