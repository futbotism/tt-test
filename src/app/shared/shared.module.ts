import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContentComponent } from './content/content.component';
import { AccordionComponent } from './accordion/accordion.component';
import { AccordionListComponent } from './accordion-list/accordion-list.component';
import { vendorDeclarations } from './vendor-declarations';

@NgModule({
  imports: [
    CommonModule,
    ...vendorDeclarations
  ],
  declarations: [ContentComponent, AccordionComponent, AccordionListComponent],
  exports: [ContentComponent, AccordionComponent, AccordionListComponent, ...vendorDeclarations],
})
export class SharedModule { }
