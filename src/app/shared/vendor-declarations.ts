import { MatSnackBarModule, MatFormFieldModule, MatInputModule } from '@angular/material';

export const vendorDeclarations = [
  MatSnackBarModule,
  MatFormFieldModule,
  MatInputModule
];
