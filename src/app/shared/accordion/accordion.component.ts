import { animate, AUTO_STYLE, style, transition, trigger } from '@angular/animations';
import { Component, forwardRef, Host, Inject, Input } from '@angular/core';

import { AccordionListComponent } from '../accordion-list/accordion-list.component';


const fadeAnimation = trigger('fadeImage', [
  transition(':enter', [
    style({ opacity: 0, height: '0' }),
    animate('300ms ease-in', style({ opacity: 1, height: AUTO_STYLE }))
  ]),
  transition(':leave', [
    animate('300ms ease-out', style({ opacity: 0, height: '0' }))
  ])
]);

@Component({
  selector: 'app-accordion',
  templateUrl: './accordion.component.html',
  styleUrls: ['./accordion.component.scss'],
  animations: [fadeAnimation]
})
export class AccordionComponent {
  @Input() title: string;
  isOpen = false;

  constructor(
    @Host() @Inject(forwardRef(() => AccordionListComponent)) public accordionList: AccordionListComponent
  ) { }

  toggleOpen() {
    this.isOpen = !this.isOpen;
    this.closePrevious();
    this.accordionList.currentlyOpen = this;
  }

  closePrevious() {
    if (this.accordionList.currentlyOpen && this.accordionList.currentlyOpen.title !== this.title) {
      this.accordionList.closePreviousAccordion();
    }
  }

  close() {
    this.isOpen = false;
  }

}
