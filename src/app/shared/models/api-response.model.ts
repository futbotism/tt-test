export interface ApiResponse<T> {
  name: string;
  content: T[];
}
