# TTest

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.2.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

# About the test
This test was built using angular, i have used some advanced patterns such as the way the accordion talks to its parent, and the form model being tied to the form group, redux etc.

## Routing
This was built using lazy loaded routes, so that the /about route is not requested from the server until you actually click on the route itself, same with the playlist js bundle. this helps with load times and package size

## Architecture
I have written the architecture in a way that it is easily extendible, everything is separated into modules, for shared, core and features. This makes the code much more maintainable and extensible over its life cycle.

I also implemented a flavour of redux known as ngxs, this is a slim redux pub/sub pattern which is great for angular apps. You can see a clear separation between the state actions and the state selections. The state selectors are also composed of one another, this can be useful for deriving state from other computed state (notice the playlist count updates as we filter)

The architecture itself aims to completely decouple the front end and backend by defining apiModel for the server response and map them to a front end instantiated object, this provides many benefits such as single responsibility and more modularity.

## Styles & look
I have implemented shared style variables and a lib which hugely improves uniformity of the design, you will notice that margins and padding are uniform across the app which creates a aesthetically consistent look and feel (ie one component isn't implementing colors or sizes 1px greater than another).

## Animations
I leveraged angulars built in animations library to create smooth transitions which enhance the user experience

## Error handling
The get request will display an error if there is a failure in the api response, to view this simply turn off the netwrok in chrome devtools and navigate away from the playlists page and back to it, turn on the devtools network press retry and note the retry action

## Tests
I have added some tests for the store to ensure the state is as expected, the state is the most important part of an app and so we want to protect it from regression as much as possible.

## What i would improve
- move more modules into a more module function, for example moving the state model into a separate class
- Add unit tests covering the component logic and template displays, and further cover the actions/selectors, eg does the filter return filtered results when the query is set?. 
- I would also have added some animations to the header and footer that run through the store when scrolling to allow more vertical space to the user.
- I would also define a folder called /testing and in here i would store all my mockServices and stubs (so they are accessible to other tests)
